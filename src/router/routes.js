
const routes = [

  {
    path: '/',
    component: () => import('components/Home/Init.vue')
   
  },
  {
    path: '/Login',
    name : "Login",
    component: () => import('components/Security/Login.vue')
   
  },
  {
    path: '/Instructions',
    name : "Instructions",
    component: () => import('components/Home/Instructions.vue')
   
  },

  {
    path: '/userValidation',
    component: () => import('components/Security/UserValidation.vue')
   
  },
  {
    path: '/termsConditions/:code',
    name : 'terms',
    component: () => import('components/Security/TermsConditions.vue')
   
  },
  {
    path: '/changePassword',
    component: () => import('components/Security/ChangePassword.vue')
   
  },
  {
    path: '/Main',
    name : "Main",
    component: () => import('components/Home/Main.vue')
   
  },
  
  {
    path: '/Home',
    component: () => import('components/Home/Home.vue'),
    name : "Home"
  },
  {
    path: '/ForgotPassword',
    component: () => import('components/Security/ForgotPassword.vue')
   
  },
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
