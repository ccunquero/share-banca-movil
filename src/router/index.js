import Vue from "vue";
import VueRouter from "vue-router";
import VueSnackbar from "vue-snack";
import "vue-snack/dist/vue-snack.min.css";
import JsonExcel from "vue-json-excel";
import VueClipboard from 'vue-clipboard2'
import VueCurrencyInput from 'vue-currency-input'
import routes from "./routes";

const pluginOptions = {
  /* see config reference */
  globalOptions: { currency: 'USD',}
}


VueClipboard.config.autoSetContainer = true // add this line
Vue.use(VueClipboard)
Vue.component("downloadExcel", JsonExcel);
Vue.use(VueCurrencyInput ,pluginOptions)
Vue.use(VueRouter);
Vue.use(VueSnackbar, { position: "bottom", time: 3000, background: "#aaa" });

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Router instance.
 */

export default function(/* { store, ssrContext } */) {
  const Router = new VueRouter({
    scrollBehavior: () => ({ x: 0, y: 0 }),
    routes,

    // Leave these as they are and change in quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  });

  Router.beforeEach((to, from, next) => {
    if (to.name == "Init") navigator.app.exitApp();
    else next();
  });

  return Router;
}
