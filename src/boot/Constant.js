//export const URL_BASE = "http://localhost:8080/bancamovilws/api/app/"; //Mio//
export const URL_BASE = "http://181.114.12.130:8787/bancamovilws/api/app/"; // puaque
//export const URL_BASE = "https://sharegt.org/bancamovilws/api/app/"; // puaque
//export const URL_BASE = "https://agenteprueba.net/bancamovilws/api/app/"; // puaque

export const Result = {
  USER_LINKED: 2, //Usuario vinculado en otro dispositivo
  USER_ERROR: 3, //Usuario incorrecto (nombre o password)
  ERROR_SERVER: 500, //Erorr en el servidor
  SUCCESS: 200, //Peticion realizada con exito
  TOKEN_ERROR: -2, //EL token no es valido
  PASSWORD_INCORRECT: -1, // La contraseña no es la correcta (En cambio de contraseña)
  VALID: 1, //Todos los datos a validar son validos
  NOT_FOUND: 404, //NO se ha encontrado
  USER_NOT_FOUND: -1, //Usuario no exite en validar token
  USER_BLOCKED: 5, //El usuario esta bloqueado
  INVALID_TOKEN_ASSOCIATED: 4, //El token no esta asociado
  USER_KEY_INVALID: 401 //El userkey no es  valido
};
export const IN_DEVICE = true;
